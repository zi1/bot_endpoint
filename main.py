import logging
import os

from dotenv import load_dotenv

from flask_helper import FlaskHelper


def main():
    # Read .env file
    load_dotenv()

    # Setup logging
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO
    )
    logging.getLogger("httpx").setLevel(logging.WARNING)

    flask_config = {
        'port': int(os.environ.get('FLASK_PORT', 5000))
    }

    # Setup and run ChatGPT and Telegram bot
    flask_helper = FlaskHelper(config=flask_config)
    flask_helper.run()


if __name__ == '__main__':
    main()
