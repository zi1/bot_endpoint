from flask import Flask, request, jsonify, send_from_directory, render_template, send_file, Response
from flask_cors import CORS
import asyncio

from uuid import uuid4
import os
import moviepy.editor as mp
import moviepy.video.fx.all as vfx


class FlaskHelper:
    """
    Class representing a Flask.
    """

    def __init__(self, config: dict):
        """
        Initializes the Flask with the given configuration
        :param config: A dictionary containing the flask configuration
        """
        self.config = config
        self.loop = asyncio.get_event_loop()
        self.app = Flask(__name__)
        CORS(self.app)

    def add_routes(self):
        @self.app.route("/crop_video", methods=['POST'])
        def crop_video():
            if request.method == "POST":
                try:
                    filename_mp4 = f'{str(uuid4())}.mp4'
                    video_link = request.json['video_link']
                    print('video_link', video_link)

                    # This is where you load in your original clip
                    video_clip = mp.VideoFileClip(video_link)
                    width = video_clip.w
                    height = video_clip.h
                    dimension = 0
                    half_dimension = 0

                    print('height', height)

                    if width > height:
                        dimension = height
                        half_dimension = (width - height) / 2
                    else:
                        dimension = width

                    clip = vfx.crop(video_clip, x1=half_dimension, y1=100, width=dimension, height=dimension)
                    clip_resized = clip.resize(height=512)
                    clip_resized.write_videofile(f'videos/{filename_mp4}', audio_codec="aac")

                    return jsonify({'video': filename_mp4})

                except Exception as error:
                    print('error', error)
                    return Response("{'error': 'Something went wrong'}", status=400, mimetype='application/json')

        @self.app.route('/videos/<path:path>')
        def send_video(path):
            return send_from_directory('videos', path)

    def run(self):
        """
        Runs the flask indefinitely until the user presses Ctrl+C
        """
        self.add_routes()

        self.app.run(host='0.0.0.0', port=self.config['port'], debug=True)
